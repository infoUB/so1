#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 3 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi

tempdir=$(mktemp -d)
#Busquem, en el directori indicat, els fitxers que acaben en 
# l'extensió desitjada i tenen mida (en bytes) més gran de la indicada.
# Copiem aquests fitxers a un directori temporal.
find $1 -type f -name "*.$2" -size +$3c -print -exec cp {} $tempdir \;

#Comprovem si el directori temporal conté algun fitxer
if [ "$(ls -A $tempdir)" ] 
then
	#Comprimim a tar, explicat a la memoria perquè ho fem aixi
	pathdir=$(pwd)
    	cd $tempdir
	tar -czf "$pathdir/fitxers.tar.gz" *
	rm -rf $tempdir
	echo "He comprimit els fitxers a fitxers.tar.gz."
	exit 0
else
	echo "No hi ha cap fitxer que compleixi amb les condicions especificades."
	rm -rf $tempdir
	exit 1
fi

