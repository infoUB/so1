#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 1 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi

#find $1 -type d ens troba recursivament tots els directoris que estan continguts en el directori paràmetre.
#Per a cada un d'aquests directoris fem el següent (dins l'-execdir bash -c):
# - Imprimim el nom del directori
# - Busquem tots els fitxers que conté
# - Llencem per stdout el nom de tots els fitxers amb la seva mida
# - Ordenem numéricament (-n) i de manera decreixent (-r) segons la mida
# - Agafem les 3 primeres línies i imprimim
find $1 -type d -printf "Directori: %p\n" -execdir bash -c 'find '{}' -maxdepth 1 -type f -printf "%s\t%P\n" | sort -nr | head -n 3' \;

exit 0


