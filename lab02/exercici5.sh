#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 2 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi

username=$1
mem=$2

#Comprovem per quin paràmetre volem ordenar
if [ $mem = 'VSZ' -o $mem = 'vsz' ]
then
    mem=5
elif [ $mem = 'RSS' -o $mem = 'rss' ]
then
    mem=6
else
    echo "El segon argument ha de ser VSZ o RSS"
    exit 1
fi

#Filtrarem primer els processos per usuari, els ordenarem en ordre descendent, imprimirem les columnes i només 10 files
results=$(ps aux | grep "^$username\>" | sort -nrk $mem | awk {'printf ("%-15d %-15d %s\n",$5,$6,$11)'} | head -n 10)
#Si aquesta variable es buida es que no té processos, sinó els imprimim
if [ -z "$results" ]
then
    echo "En $username no té cap procés associat"
    exit 1
fi
echo -e 'VSZ \t\t RSS \t\t Proces'
echo "$results"
exit 0
