#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 1 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi

IFS=":"

# Agafem la línia del fitzer corresponent a l'usuari en forma d'array
user_info=( $( grep -E "^${1}" /etc/passwd ) )

# Si s'ha agafat alguna línia (sii l'usuari existeix)
if [ $? -eq 0 ]
then
	# Busquem tots els grups on hi ha l'usuari, agafem els noms i les ID's i apliquem format amb awk i afegim les comes amb sed.
	user_groups=$( grep -E "[:,]${1}(,|$)" /etc/group | awk -F ":" '{printf("%d (%s) ", $3, $1)}' | sed -re "s/\) ([0-9])/\), \1/g" )
	
	# Imprimim la informació recollida
	echo "Nom d'usuari: ${user_info[4]}"
	echo "Identificador d'usuari: ${user_info[2]}"
	echo "Grups als que pertany l'usuari: $user_groups"
	echo "Directori arrel de l'usuari: ${user_info[5]}"
	echo "Intèrpret per defecte: ${user_info[6]}"
	exit 0
else
	echo "Aquest usuari no existeix en aquest ordinador"
	exit 1
fi
