#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 3 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi


#Busquem recursivament tots els fitxers dins el directori especificat
#Per a cada fitxer, imprimim el seu nom, fem un grep de les strings
#	que contenen els dos paràmetres i contem el nombre de coincidències
find $1  -type f -printf "%p " -exec bash -c "grep -oiE "${2}[^[:space:]]*${3}" {} | wc -l " \;

