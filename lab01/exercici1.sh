#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 1 ]
then
    echo "Nombre de paràmetres incorrecte"
    exit 1
fi

#Comprovem que el paràmetre sigui un directori
if [ ! -d $1 ]
then
    echo "El paràmetre no és un directori o no existeix"
    exit 2
fi

#Creem fitxer temporal
tempfile=$(mktemp)
#Afegim al fitxer una llista ordenada del contingut del directori
ls -AlS $1 > $tempfile
#Agafem els noms dels elements
names=($(awk '{print $9}' $tempfile))
#Agafem la mida dels elements
sizes=($(awk '{print $5}' $tempfile))
#Borrem el fitxer temporal
rm $tempfile
len=${#names[*]}

#Recorrem la llista fins trobar un fitxer, que sera el més gran
for (( i=0; i<$len; i++ ))
do
    if [ -f $1/${names[i]} ]; then
        echo "Fitxer més gran: ${names[i]}, ${sizes[i]}"
        exit 0
    fi
done

#Si sortim vol dir que no hi ha cap fitxer
echo "El directori especificat no conte cap fitxer"
exit 0
 
