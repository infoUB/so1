#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 3 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi

directori=$1
cadena1=$2
cadena2=$3
len_cadena1=${#cadena1}
len_cadena2=${#cadena2}
sum=0
n_files=0
IFS=$' \t\n'
#Creem fitxer temporal
tempfile=$(mktemp)
#Afegim al fitxer llista dels elements del directori
ls -Al $1 > $tempfile
#Extraiem noms dels arxius en una llista
elements=$(awk '{print $9}' $tempfile)
for i in $elements
do
	if [ -f $directori/$i ]; then #En cas de ser un fitxer l'analitzem
        (( n_files++ ))
		IFS=$' \t-,:;.*\'()[]¿?¡!"\r\n' #Canviem el IFS per separar les paraules de simbols
		words=$(cat "$directori/$i")
		for j in $words #Recorrem totes les paraules
		do
            #Mirem si les dues cadenes coincideixen amb modificacio de strings
			if [[ "${j:0:$(( $len_cadena1 ))}" = "$cadena1" ]] && [[ "${j:(( ${#j}-$len_cadena2 )):(( $len_cadena2 ))}" = "$cadena2" ]]
			then
				echo "$i: $j" #Imprimim la paraula trobada
				(( sum++ ))
			fi
		done
        IFS=$' \t\n' #Tornem a canviar el IFS per agafar be els fitxers
	fi
done

#Finalment, comprovem si hi ha fitxers al directori i imprimim el output
if [ $n_files = 0 ]
then
    echo "El directori especificat no conté cap fitxer"
else
    echo "Total coincidències: $sum"
fi
rm $tempfile
exit 0
