#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 3 ]
then
	echo "Nombre de paràmetres incorrecte"
	exit 1
fi

directori=$1
cadena1=$2
cadena2=$3
len_cadena1=${#cadena1}
len_cadena2=${#cadena2}
sum=0
#Creem fitxer temporal
#Afegim al fitxer llista dels elements del directori
#Extraiem noms dels arxius en una llista
elements=$(ls $directori)
for i in $elements
do
    if [ -f $directori/$i ]; then
		IFS=$' \t,:;.*\'()[]¿?¡!"\n'
		words=$(cat "$directori/$i")
		for j in $words
		do

			if [[ "${j:0:$(( $len_cadena1 ))}" = "$cadena1" ]] && [[ "${j:(( ${#j}-$len_cadena2 )):(( $len_cadena2 ))}" = "$cadena2" ]]
			then
				echo "$i: $j"
				(( sum++ ))
			fi
		done
		IFS=$' \t\n'
	fi
done
echo $sum
exit 0
