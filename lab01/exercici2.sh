#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 1 ]
then
    echo "Nombre de paràmetres incorrecte"
    exit 1
fi

string=$1
#Imprimim la paraula per primer cop, sense salt de linia
echo -n "Palíndrom: $string"
#Comencem el bucle al 2, ja que la len de la paraula és 1 més de la última posició i tampoc volem l'última lletra
for (( i=2; i<=${#string}; i++ ))
do
    #Imprimim lletra per lletra (desde el final) sense salts de linia
    echo -n "${string:$(( ${#string} - $i )):1}"
done
echo "" #Fem un salt de linia
exit 0
