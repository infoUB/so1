#! /bin/bash

#Comprovem que el nombre de paràmetres sigui >= 2
if [ $# -lt 2 ]
then
    echo "Nombre de paràmetres incorrecte"
    exit 1
fi

#Generem un array pels contadors de cada extensió
for ((i=2;i<=$#;i++))
do
    count[$i]=0
done

#Iterem sobre els fitxers i directoris
contents=$(ls $1)
for i in $contents
do
    if [ -f $1/$i ]                 #Si es tracta d'un fitxer
    then
        ext="${i##*.}"              #Obtenim l'extensió del fitxer actual
        for ((j=2;j<=$#;j++))       #Per a cada extensió a comprovar
        do
            if [ $ext = ${!j} ]     #Si l'extensió és alguna de les que busquem
            then 
                (( count[j]++ ))    # augmentem el comptador en 1
                break
            fi
        done
    elif [ -d $1/$i ]               #Si es tracta d'un directori
    then
        ./$0 "$1/$i" "${@:2}"       # aleshores executem recursivament aques script amb el nou directori
    fi

done

#imprimim el nom del directori i els contadors e fitxer
echo -n "$1"
for ((i=2;i<=$#;i++))
do
    echo -n " ${!i}:${count[$i]}"
done
echo ""
exit 0
