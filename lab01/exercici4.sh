#!/bin/bash

#Comprovem que el nombre de paràmetres sigui el correcte
if [ $# -ne 2 ]
then
    echo "Nombre de paràmetres incorrecte"
    exit 1
fi

#Donem nom a les variables
directori=$1
extensio=$2
len_extensio=${#extensio}
sum=0

#Creem directori temporal
tempfile=$(mktemp)
tempdirectory=$(mktemp -d)

#Afegim al fitxer la llista dels elements del directori
ls -Al $directori > $tempfile
#Extraiem noms dels arxius en una llista
elements=$(awk '{print $9}' $tempfile)
rm $tempfile

#Recorrem els elements
for i in $elements
do
    #Comprovem si es un fitxer amb l'extensió que toca
	if [ -f $directori/$i -a  "${i:$((${#i}-$len_extensio))}" = "$extensio" ]; then
		cp $directori/$i $tempdirectory/$i #El copiem al directori temporal
		(( sum++ )) #Sumem 1 al nombre de fitxers
	fi
done
if [ $sum -ne 0 ]; then #Si tenim fitxers, llavors comprimim
    pathdir=$(pwd)
    cd $tempdirectory
	tar -czf "$pathdir/fitxers.tar.gz" * #Comprimim a tar, explicat a la memoria perque aixi
	rm -rf $tempdirectory
    echo "He comprimit tots els fitxers d'extensió $extensio. Els tens a fitxers.tar.gz"
	exit 0
fi

rm -rf $tempdirectory
echo "El directori no conté cap fitxer amb l'extensió especificada"
exit 1
