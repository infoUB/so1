#!/bin/bash

if [ $# -ne 1 ]
then 
    echo "Nombre incorrecte de parametres"
    exit 1
fi

ls -l $1 > ls.tmp
bytes=($(awk '{print $5}' ls.tmp))
len=${#bytes[*]}

i=0
sum1=0

while [ $i -lt $len ]
do
    sum1=$(($sum1 + ${bytes[i]}))
    (( i++ ))
done
rm ls.tmp
echo $sum1

exit 0
