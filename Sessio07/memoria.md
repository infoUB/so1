# Sessió 7 - Eficiència read i fread 
## Temps d'execució
Els programes s'han executat sobre un equip GNU/Linux amb processador Intel Core 2 Duo P8400 de 2 nuclis a  2.26GHz, 4GB de memòria ram, disc dur SSD.

### io_system_call

|**Block size**| 64 | 1024 | 4096 | 16384 | 65336 | 262144 |
|--| -- | -- | -- | -- | -- | -- | -- |
|**real**|1m30.145s|0m16.707s|0m16.964s|0m16.686s|0m17.582s|0m17.021s|
|**user**|0m22.591s|0m2.209s|0m0.672s|0m0.240s|0m0.128s|0m0.022s|
|**sys**|1m7.402s|0m8.969s|0m4.742s|0m5.038s|0m4.617s|0m3.273s|


### io_stdio_fread

|**Block size** | 64 | 1024 | 4096 | 16384 | 65336 | 262144 |
|--| -- | -- | -- | -- | -- | -- | -- |
|**real**|0m16.516s|0m16.954s|0m17.166s|0m16.801s|0m16.647s|0m16.598s|
|**user**|0m7.924s|0m2.055s|0m1.422s|0m0.663s|0m0.301s|0m0.069s|
|**sys**|0m3.956s|0m4.710s|0m5.046s|0m4.535s|0m4.085s|0m3.498s|

## Anàlisi dels resultats
El que ens interessa realment és el temps donat pels camps "**user**" i especialment "**sys**". Com podem legir en el manual de time, **user** és el temps que la CPU ha estat executant codi en mode usuari i **sys** és el temps en que ho ha estat fent en mode nucli. Podem veure que el temps de **sys** quan fem les crides a sistema va variant molt. 

Cada vegada que fem una crida a  ```read```, el procés haurà de passar a mode nucli i lligir immediatament a memòria. Amb blocs de 64Kb es passa un total de 1 minut i 7.4 segons, ja que s'haurà de fer una crida a sistema per a cada bloc. A mesura que els blocs es van fent més grans, aquest temps anirà disminuïnt, ja que cada cop caldrà fer menys crides a sistema per llegir el total dels 4 GB. El temps en mode usuari també disminuirà, ja que haurem de fer menys iteracions del while. 

Si observem els temps **sys** fent crides a ```fread```, veiem que són tots bastant semblants. Això és degut a que ```fread``` té un buffer intern que s'omple amb una part del fitxer del qual es llegeixen els blocs, i no es fa la crida a ```read``` fins que necessitem blocs que no són al buffer. Com que la mida del buffer és la mateixa per a totes les mides de bloc, es faràn pràcicament sempre el mateix nombre de cirdes a sistema. On sí que trobem variació és en el camp **user**, ja que, com més gran sigui la mida de bloc, menys iteracions del while farà.

Amb ```write``` i ```fwrite``` passaria el mateix. ```write``` escriuria directament el bloc a disc mentre que ```fwrite``` només escriuria a disc un cop el buffer fos ple. Però, en aques cas s'escriu a STDOUT, de manera que no cal fer escriptures a disc i, per tant, no hi ha gaire diferència.
