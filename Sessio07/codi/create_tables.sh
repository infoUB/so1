#!/bin/bash

tempfile2=$(mktemp)
taules=$(mktemp)
sizes=(64 1024 4096 16384 65336 262144)

if [ ! -f big_file.bin ]
then   
    gcc create_big_file.c -o create_big_file
    ./create_big_file
fi

if [ ! -f io_stdio_fread ]
then
    gcc io_stdio_fread.c -o io_stdio_fread
fi

if [ ! -f io_system_call ]
then
    gcc io_system_call.c -o io_system_call
fi


for size in ${sizes[@]}
do
    tempfile=$(mktemp)
    echo "$size"
    sync && echo 3 > /proc/sys/vm/drop_caches
    { time ./io_system_call $size < big_file.bin > /dev/null; } 2> $tempfile
    awk '{print $2}' $tempfile | tail -n 3 > $tempfile2
    paste -d'|' $taules $tempfile2 > $tempfile
    taules=$tempfile
done

echo "| 64 | 1024 | 4096 | 16384 | 65336 | 262144 |"
echo "| -- | -- | -- | -- | -- | -- | -- | "
cat $tempfile

tempfile2=$(mktemp)
taules=$(mktemp)

for size in ${sizes[@]}
do
    tempfile=$(mktemp)
    echo "$size"
    sync && echo 3 > /proc/sys/vm/drop_caches
    { time ./io_stdio_fread $size < big_file.bin > /dev/null; } 2> $tempfile
    awk '{print $2}' $tempfile | tail -n 3 > $tempfile2
    paste -d'|' $taules $tempfile2 > $tempfile
    taules=$tempfile
done

echo "| 64 | 1024 | 4096 | 16384 | 65336 | 262144 |"
echo "| -- | -- | -- | -- | -- | -- | -- | "
cat $tempfile

   
