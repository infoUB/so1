#include <stdlib.h>
#include <stdio.h>

int main()
{
  int i;
  int *p;

  p = calloc(10, sizeof(int));
    
  for(i = 0; i < 10; ++i)
  {
      printf("%d\n", *(p+i));
  }
  printf("Adreça vella: %p\n", p);
  p = realloc(p, 20*sizeof(int));
  printf("Adreça nova: %p\n", p);

  for(i = 0; i < 20; i++)
    p[i] = i;

  for(i = 0; i < 20; i++)
    printf("%d\n", p[i]);

  free(p);

  return 0;
}
