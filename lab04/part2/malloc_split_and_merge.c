#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "struct.h"


#ifndef __DEBUG__
#define __DEBUG__

#ifdef DEBUG
#define debug(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#else
#define debug(fmt, ...) ((void)0)
#endif

#endif 


/* El fet de fer servir variables 'static' fa que les
* variables que hi ha a continuacio nomes es puguin
* fer servir des d'aquest fitxer. Implementeu tot el
* codi en aquest fitxer! */
static p_meta_data first_element = NULL;
static p_meta_data last_element  = NULL;

/* La seguent variable es la que permet bloquejar un 
* codi en cas que hi hagi multiples fils. Amb aixo ens
* assegurem que nomes un fil pugui entrar dintre d'un
* codi que es bloqueja. Se us demana no tocar la part
* bloquejada si voleu provar el codi amb executables
* com el kate, kwrite, ... */
static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

static long total_allocated_mem = 0;

#define ALIGN8(x) (((((x)-1)>>3)<<3)+8)
#define MAGIC     0x87654321
#define DIVIDE_BOUND_SIZE  12288


p_meta_data check_and_merge(p_meta_data meta)
{

    debug("\nBegin free at %p: %ld bytes\n", meta, meta->size_bytes);
    
    p_meta_data adjacent = meta->next;
    if(adjacent && adjacent->available)
    {
        //Augmentem la mida 
        debug("\tMerging next block at %p: %ld + %ld bytes\n", adjacent, adjacent->size_bytes, SIZE_META_DATA);
        meta->size_bytes += adjacent->size_bytes + SIZE_META_DATA; 
        
        //Canviem el punter del next
        meta->next = adjacent->next;
        
        if(adjacent == last_element)
        {
            last_element = meta;
        }
        else
        {
            //Fem intercanvi de la resta de punters si és necessari
            adjacent->next->previous = meta;        
        }
    }
    
    //Fem el simètric per davant (això ho podem fer en un sol if!!!)
    if(meta != first_element && meta->previous->available)
    {
        adjacent = meta->previous;	
        debug("\tMerging previous block at %p: %ld + %ld bytes\n", adjacent, adjacent->size_bytes, SIZE_META_DATA);
        adjacent->size_bytes += meta->size_bytes + SIZE_META_DATA;
        
        adjacent->next = meta->next;
        
        if(meta == last_element)
        {
            last_element = adjacent;
        }
        else
        {
            meta->next->previous = adjacent;
        }
        return adjacent;
    }
    return meta;
}


void subdivide_block(p_meta_data block, size_t requested_size)
{
    //Comprovem si sobra suficient espai (abans comprovem si podem fer la resta)
    p_meta_data new_meta_data;
    if(block->size_bytes > requested_size && block->size_bytes - requested_size >= 12288)
    {
        new_meta_data = (p_meta_data)( ((void *)block) + SIZE_META_DATA + requested_size + 8);
        debug("Dividing block at %ld \n", new_meta_data->size_bytes);
        new_meta_data->size_bytes = block->size_bytes - requested_size - SIZE_META_DATA;
        new_meta_data->available = 1;
        new_meta_data->magic = MAGIC;
        new_meta_data->next = block->next;
        new_meta_data->previous = block;
        block->size_bytes = requested_size;
        if(block == last_element)
        {
            last_element = new_meta_data;
        }
        else
        {
            block->next->previous = new_meta_data;
        }
        
        block->next = new_meta_data;
        debug("Dividing block at %p of size %ld + %ld bytes into block %p of size %ld \n", block, block->size_bytes, SIZE_META_DATA, new_meta_data, new_meta_data->size_bytes);
    }
    
}




/* Aquesta funcio permet buscar dintre de la llista un block lliure
* que tingui la mida especificada */
p_meta_data search_available_space(size_t size_bytes) {
    p_meta_data current = first_element;

    while (current && !(current->available && current->size_bytes >= size_bytes)) 
        current = current->next;

    return current;
}

/* Aquesta funcio es la que fa la crida a sistema sbrk per demanar
* al sistema operatiu un determinat nombre de bytes. Al principi del
* tot s'afegiran les metadades, que l'usuari no fa servir. */
p_meta_data request_space(size_t size_bytes) 
{
    p_meta_data meta_data;

    meta_data = (void *) sbrk(0);

    //Línies de l'apartat 7
    if(size_bytes < 122880)
    {
        debug("Creating 122880B block!!!\n");
        size_bytes = 122880; // Ha de ser "=" o "+="??
    }
    
    if (sbrk(SIZE_META_DATA + size_bytes) == (void *) -1)
    {
        return (NULL);
    }
    meta_data->size_bytes = size_bytes;
    meta_data->available = 0;
    meta_data->magic = MAGIC;
    meta_data->next = NULL;
    meta_data->previous = NULL;

    return meta_data;
}

/* Allibera un bloc. L'argument es el punter del bloc a alliberar.
* La funcio free accedeix a les metadades per indicar que el bloc
* es lliure. */
void free(void *ptr)
{
    p_meta_data meta_data;

    if (!ptr) 
        return;

    // Bloquegem perque nomes hi pugui entrar un fil
    pthread_mutex_lock(&mutex);
    meta_data = (p_meta_data) (ptr - SIZE_META_DATA);

    if (meta_data->magic != MAGIC) {
        debug("ERROR free: value of magic not valid\n");
        exit(1);
    } 

    meta_data->available = 1;
    meta_data = check_and_merge(meta_data);
    debug("Free at %p: %zu bytes\n\n", meta_data, meta_data->size_bytes);
    // Desbloquegem aqui perque altres fils puguin entrar
    // a la funcio
    pthread_mutex_unlock(&mutex);
}

/* La funcio malloc per demanar un determinat nombre de bytes. La
* funcio busca primer a la seva llista si hi ha algun bloc lliure
* de la mida demanada. Si el troba, el retorna a l'usuari. Si no
* en troba cap de la mida demanada, fa una crida a request_space
* la qual internament fa una crida a sistema. */
void *malloc(size_t size_bytes) 
{
    void *p, *ptr;
    p_meta_data meta_data;
    if (size_bytes <= 0) {
        return NULL;
    }

    // Reservem una mida de memoria multiple de 8. Es
    // cosa de l'electronica de l'ordinador que requereix
    // que les dades estiguin alineades amb multiples de 8.
    size_bytes = ALIGN8(size_bytes);
    debug("Malloc %zu bytes\n", size_bytes);

    // Bloquegem perque nomes hi pugui entrar un fil
    pthread_mutex_lock(&mutex);

    meta_data = search_available_space(size_bytes);

    if (meta_data) { // free block found 
            meta_data->available = 0;
    } else {     // no free block found 
        meta_data = request_space(size_bytes);
        if (!meta_data)
                return (NULL);
        if (last_element) // we add to the last element of the list
                last_element->next = meta_data;
        meta_data->previous = last_element;
        last_element = meta_data;

        if (first_element == NULL) // Is this the first element ?
                first_element = meta_data;
        
        //Línia de l'apartat 6
        subdivide_block(meta_data, size_bytes);
    }
    

    p = (void *) meta_data;

    // Desbloquegem aqui perque altres fils puguin entrar
    // a la funcio
    pthread_mutex_unlock(&mutex);

    //Imprimim el total de memòria reservada fins ara
    total_allocated_mem += size_bytes + SIZE_META_DATA;
    debug("Malloc'd a total of %zu bytes\n", total_allocated_mem);

    // Retornem a l'usuari l'espai que podra fer servir.
    ptr = p + SIZE_META_DATA;
    return ptr; 
}

void *calloc(size_t nelem, size_t elsize)
{
    void *ptr;

    ptr = malloc(nelem*elsize);
    if(ptr)
    {
        memset(ptr, 0, nelem*elsize);
    }
    debug("Calloc done at %p\n", ptr);
    return ptr;    
}

void *realloc(void *ptr, size_t size_bytes)
{
    void *new_ptr;

    if(!ptr)
    {
        return malloc(size_bytes);
    }

    p_meta_data meta_data;
    meta_data = (p_meta_data) (ptr - SIZE_META_DATA);

    if(meta_data->size_bytes < size_bytes)
    {
        new_ptr = malloc(size_bytes);

        if(new_ptr)
        {
            debug("Realloc from %p to %p\n", ptr, new_ptr);
            memcpy(new_ptr, ptr, meta_data->size_bytes);
            free(ptr);
        }
        return new_ptr;
    }

    return ptr;
}


