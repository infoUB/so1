#!/bin/bash

if [ $# -ne 1 ]
then
   echo "Nombre de parametres incorrecte"
   exit 1
fi

fitxer=$1
count=0
paraules=$(ls $fitxer )
for i in $paraules 
do
  count=$(($count + 1))
done
echo $count
echo $((ls $fitxer ) | wc -l)

exit 0

