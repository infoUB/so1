#!/bin/bash

# Comprovem que hi hagi un únic paràmetre
if [ $# -ne 1 ] 
then
   echo "Nombre de parametres incorrecte"
   exit 1
fi

# Comprovem que el paràmetre sigui un fitxer
if [ ! -f $1 ]
then
    echo "El paràmetre no és un fitxer"
    exit 1
fi

fitxer=$1
exist=0
not_exist=0
lines=$(cat  $fitxer )

for i in $lines 
do
    if [ -f $i ] || [ -d $i ]
    then
        #Augmentem el contador de fitxers/directoris que existeixen
        exist=$(($exist + 1))
    else
        #Augmentem el contador de fitxers/directoris que no existeixen
        not_exist=$(($not_exist + 1))
    fi
done

echo "Existeixen: $exist"
echo "No existeixen: $not_exist"

exit 0

