#!/bin/bash

if [ $# -ne 1 ]
then
    echo "Nombre de paràmetres incorrecte"
    exit 1
fi

if [ ! -d $1 ]
then
    echo "El paràmetre no és un directori"
    exit 1
fi

for i in $(find $1 -type d)
do 
    echo -n "Directori: $i  nfiles: "
    find $i -maxdepth 1 -type f | wc -l
done
exit 0
