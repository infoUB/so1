#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>


/* definició de la funció debug() per fer print. Només s'executa compilant amb el flag -DDEBUG */
#ifndef __DEBUG__
#define __DEBUG__

#ifdef DEBUG
#define debug(fmt, ...) fprintf(stderr, fmt, ##__VA_ARGS__)
#else
#define debug(fmt, ...) ((void)0)
#endif

#endif 


/* Definicions per debugar. S'HAN DE TREURE I DEFINIR ELS SEUS VALORS */
#define MAX_SEC 5 //Segons contarà des de 0 fins a MAX_SEC inclòs
#define MAX_MIN 2 //Minuts contarà des de 0 fins a MAX_MIN inclòs

#define SIZE 100 //Definició del size del buffer


/* VARIABLES GLOBALS */

//Variable per definir l'estat print.
// Ha de ser volatile perquè el compilador no la rebenti
// i sig_atomic_t per coses rares de la senyal
static volatile sig_atomic_t print = 0;

pid_t pid_sec, pid_min, pid_hour;



/* SIGNAL HANDLERS */

void sig_alrm(int sig)
{
	//printf("Ha passat 1 segon\n");
}

void show(int sig)
{
	print = 1;
}

void recieve(int sig){}

void start(int sig)
{
	// Canviem el singal_handler
	signal(SIGUSR1, show);
}

void stop(int sig)
{
	char buffer[SIZE];
	sprintf(buffer, "Tancant rellotge...\n");
	write(1, buffer, strlen(buffer));
	kill(pid_sec, SIGTERM);
	kill(pid_min, SIGTERM);
	kill(pid_hour, SIGTERM);
	exit(0);	
}


/* MAIN */

int main(void)
{
	int i, num_values, value, sum, fd[2];
	int ret, parent_pid, child_pid;
	int fd_sec[2], fd_min[2], fd_hour[2];
	//int pid_sec, pid_min, pid_hour;
	char buffer[SIZE];

	/* Inicialitzem les pipes */
	pipe(fd_sec);
	pipe(fd_min);
	pipe(fd_hour);

	ret = fork();

	if (ret == 0) //Seconds child
	{     
		int seconds;
		seconds = 0;

		pid_sec = getpid();
		debug("Seconds process PID: %d, with parent %d\n", pid_sec, getppid());

		read(fd_sec[0], &pid_min, sizeof(int)); //Llegim pid_min que envia parent de proces minuts
		debug("Seconds recieved min_pid: %d\n", pid_min);

		signal(SIGUSR1, start); //Esperem senyal d'inici
		pause();

		signal(SIGALRM, sig_alrm); //Definim el handler per l'alarma
		while(1)
		{
			alarm(1); //Definim alarma a 1 segon
			pause(); //Esperem signal
			if(print == 1) //Si ha estat SIGUSR1, hem d'imprimir
			{
				debug("Print seconds\n");
				write(fd_sec[1], &seconds, sizeof(int));
				print = 0;
			}
			else //Si ha estat SIGALRM
			{
				debug("New second\n");
				seconds++;
				if(seconds == MAX_SEC)
				{
					debug("Sending new minute\n");
					kill(pid_min, SIGUSR2); //Enviem SIGUSR2 al fork minuts
					seconds = 0;
				}
			}
		}
	}
	else //Parent
	{
		pid_sec = ret;

		ret = fork();
		if(ret == 0) //Minutes child
		{
			pid_min = getpid();
			debug("Minutes process PID: %d, with parent %d\n", pid_min, getppid());           


			int minutes;
			minutes = 0;

			read(fd_min[0], &pid_hour, sizeof(int)); //Llegim el PID del proces hores
			debug("Minutes recieved pid_hour: %d\n", pid_hour);
			
			signal(SIGUSR1, show);
			signal(SIGUSR2, recieve);
			while(1)
			{
				pause();
				if(print == 1)
				{
					debug("Print minutes\n");
					write(fd_min[1], &minutes, sizeof(int));
					print = 0;
				}
				else
				{
					debug("Recieved new minute\n");
					minutes++;
					if(minutes == MAX_MIN)
					{
						debug("Send new hour\n");
						kill(pid_hour, SIGUSR2);
						minutes = 0;
					}

				}
			}
		}
		else
		{
			pid_min = ret;

			ret = fork();
			if(ret == 0) //Hours child
			{
				pid_hour = getpid();
				debug("Hours process PID: %d, with parent %d\n", pid_hour, getppid());

				int hours;
				hours = 0;

				signal(SIGUSR1, show);
				signal(SIGUSR2, recieve);
				while(1)
				{
					pause();
					if(print == 1)
					{
						debug("Print hours\n");
						write(fd_hour[1], &hours, sizeof(int));
						print = 0;
					}
					else
					{
						debug("Recieved new hour\n");
						hours++;
					}
				}

			}
			else //Parent
			{
				int seconds, minutes, hours;
				pid_hour = ret;

				sprintf(buffer, "Parent PID: %d\n", getpid());
				write(1, buffer, strlen(buffer));

				write(fd_sec[1], &pid_min, sizeof(int)); //Enviem pid_min a el fork de segons
				write(fd_min[1], &pid_hour, sizeof(int)); //Enviem pid_hour al fork de minuts


				signal(SIGUSR1, start); //Definim handler de SIGUSR1 i esperem a rebre-la
				signal(SIGTERM, stop);
				pause();
				// Imprimim status
				char buffer[SIZE];
				sprintf(buffer, "Iniciant rellotge\n");
				write(1, buffer, strlen(buffer));


				kill(pid_sec, SIGUSR1); //Enviem senyal d'inici a fork segons
				while(1)
				{
					pause();        
					if(print == 1)
					{
						//Enviem senyal per imprimir als altres processos
						kill(pid_sec, SIGUSR1);
						kill(pid_min, SIGUSR1);
						kill(pid_hour, SIGUSR1);

						//Llegim les seves respostes
						read(fd_sec[0], &seconds, sizeof(int));
						read(fd_min[0], &minutes, sizeof(int));
						read(fd_hour[0], &hours, sizeof(int));

						//Escrivim els resultats per pantalla
						sprintf(buffer, "%02d:%02d:%02d\n", hours, minutes, seconds);
						write(1, buffer, strlen(buffer));
						print = 0;
					}
					else
					{
						break;
					}
				}
			}
		}
	}
	wait(NULL);
	return 0;
}
